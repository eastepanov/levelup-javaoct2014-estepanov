package my.level.up.lab1;


public class Tiger extends Animal {
    private int fang;
    private int claws;

    public Tiger() {

    }

    public Tiger(String typeOfAnimals, int quantityOfLegs, int weight, char meal, int fang, int claws) {
        super(typeOfAnimals, quantityOfLegs, weight, meal);
        this.fang = fang;
        this.claws = claws;
    }


    public int getClaws() {
        return claws;
    }

    public void setClaws(int claws) {
        this.claws = claws;
    }

    public int getFang() {
        return fang;
    }

    public void setFang(int fang) {
        this.fang = fang;
    }

    @Override
    public void eat(char meal) {   //override
        System.out.println(super.getTypeOfAnimals() + ": I`m eating " + meal + " by means of my " +
                claws + " claws and " + fang + " fangs!");
    }

    @Override
    public void drink() {
        System.out.println(super.getTypeOfAnimals() + ": I`m drinking now!");
    }

    @Override
    public String toString() {
        return super.toString() + "\nFang = " + fang +
                "\nClaws = " + claws;
    }

    public void hunt(int fang, int claws) {
        System.out.println(super.getTypeOfAnimals() + ": I`m hunting by means of my " +
                fang + " fangs" + " and " + claws + " claws!");
    }


    public void climbOnTree(int claws) {
        System.out.println(super.getTypeOfAnimals() +
                ": I`m climbing on tree by means of my " + claws);
    }

    public void setWeight(String weight) { //overloading
        if(weight == "Tiger"){
            return;
        }
        System.out.println("Tiger`s weight is " + weight);
    }

}
