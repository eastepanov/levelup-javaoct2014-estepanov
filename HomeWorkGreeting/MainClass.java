package model;

/**
 * Created by SMULL on 22.10.2014.
 */
public class MainClass {

    public static void main(String[] args) {
        Greeting greetingChinese = new ChineseGreeting() {
            @Override
            public void greet() {
                System.out.println("Ni hao!");
            }

            @Override
            public void greetWithName(String name) {
                System.out.println("Ni hao, " + name);
            }
        };

        MyGreet myGreet = new MyGreet();
        printGreeting(myGreet);

        System.out.println("========================================");

        FrenchGreeting greetingFrench =  myGreet.new Inner();
        printGreeting(greetingFrench);

        System.out.println("========================================");

        printGreeting(greetingChinese);
    }

    public static void printGreeting(Greeting greet){
        greet.greet();
        greet.greetWithName("Zhenia");
    }
}
