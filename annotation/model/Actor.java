package com.levelup.model;

/**
 * Created by denis_zavadsky on 12/6/14.
 */
public class Actor {

    public static final String INSERT_ACTOR = "INSERT INTO Actor(role, userId) VALUES(?,?)";

    private Long id;
    private String role;
    private Long userId;

    private User user;

    public Actor() {
    }

    public Actor(Long id, String role, Long userId) {
        this.id = id;
        this.role = role;
        this.userId = userId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
