package com.model;


import java.util.Comparator;


public  class MyClass implements Comparator<Integer> {

    private int numberOfOne;
    private int numberOfTwo;

    public MyClass() {
    }

    public MyClass(int numberOfOne, int numberOfTwo) {
        this.numberOfOne = numberOfOne;
        this.numberOfTwo = numberOfTwo;
    }

    public int getNumberOfOne() {
        return numberOfOne;
    }

    public void setNumberOfOne(int numberOfOne) {
        this.numberOfOne = numberOfOne;
    }

    public int getNumberOfTwo() {
        return numberOfTwo;
    }

    public void setNumberOfTwo(int numberOfTwo) {
        this.numberOfTwo = numberOfTwo;
    }

    @Override
    public  int compare(Integer o1, Integer o2) {
        System.out.println("MyClass");
        int res = ((o1 % 31) - (o2 % 31));
        if(res < 0)
            return -1;
        else if(res > 0)
            return 1;
        return 0;
    }

    public static void main(String[] args) {

        MyClass myClass = new MyClass(32,37);
        System.out.println(myClass.compare(myClass.getNumberOfOne(),myClass.getNumberOfTwo()));


        Compared compared = new Compared() {
            @Override
            public int compare(Integer o1, Integer o2) {
                System.out.println("AnonymousInner");
                int res = ((o1 % 31) - (o2 % 31));
                if(res < 0)
                    return -1;
                else if(res > 0)
                    return 1;
                return 0;
            }
        };

        System.out.println(compared.compare(myClass.getNumberOfOne(), myClass.getNumberOfTwo()));

    }

}
