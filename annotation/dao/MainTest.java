package com.levelup.dao;

import com.levelup.model.Account;
import com.levelup.model.User;

import java.util.Date;

/**
 * Created by denis_zavadsky on 12/6/14.
 */
public class MainTest {

    public static void main(String[] args) {
        MainTest test = new MainTest();
        test.doTest();
    }

    public void doTest(){
        AuthController authController = new AuthController();
        //Account account = authController.registerUser("test","123","Ivan","Ivanov",new Date());
        //authController.insertUser("Ruslan","Dubinin",new Date(14-8-1989));
        authController.printUser(26l);
        //Account account = authController.loginUser("test2","123");
        authController.close();
    }
}
