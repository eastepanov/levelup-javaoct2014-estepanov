package my.level.up.lab1;

public class Elephant extends Animal {
    private int tusks;
    private char trunk;

    public Elephant() {

    }

    public Elephant(String typeOfAnimals, int quantityOfLegs, int weight, char meal, int tusks, char trunk) {
        super(typeOfAnimals, quantityOfLegs, weight, meal);
        this.trunk = trunk;
        this.tusks = tusks;
    }

    public char getTrunk() {
        return trunk;
    }

    public void setTrunk(char trunk) {
        this.trunk = trunk;
    }

    public int getTusks() {
        return tusks;
    }

    public void setTusks(int tusks) {
        this.tusks = tusks;
    }

    @Override
    public void eat(char meal) {     //override
        System.out.println(super.getTypeOfAnimals() +
                ": I`m eating " + meal + " by means of " + trunk);
    }

    @Override
    public void drink() {
        System.out.println(super.getTypeOfAnimals() +
                ": I`m drinking now!");
    }

    @Override
    public String toString() {
        return super.toString() +
                "\nTusks = " + tusks +
                "\nTrunk = " + trunk;
    }

    void takeShower(char trunk) {
        System.out.println(super.getTypeOfAnimals() +
                ": I`m taking a shower by means of my " + trunk);
    }

    void defend(int tusks) {
        System.out.println(super.getTypeOfAnimals() +
                ": I`m defending by means of my " + tusks + " from predator!");
    }

    public void setWeight(char weight) { //overloading
        if(weight == '0'){
            return;
        }
        System.out.println("Elephant`s weight is " + weight);
    }

}
