package model.ArrayTask2;

/**
 * Created by SMULL on 26.10.2014.
 */
public class ArrayTask2 {
    public static void main(String[] args) {

        ArrayTask2 arrayTask2 = new ArrayTask2();
        System.out.println(arrayTask2.makeBricks(3, 1, 8));
        System.out.println("============================");

        System.out.println(arrayTask2.noTeenSum(16, 18, 19));
        System.out.println("============================");

        System.out.println(arrayTask2.blackJack(13,5));
    }



    public boolean makeBricks(int quantityOfBricksWithLength1, int quantityOfBricksWithLength5, int onlyLine){
        return (quantityOfBricksWithLength1 * 1 + quantityOfBricksWithLength5 * 5) >= onlyLine;
    }

    public int noTeenSum(int a, int b, int c){
        return fixTeen(a) + fixTeen(b) + fixTeen(c);
    }

    private int fixTeen(int n){
        if(n >= 13 && n <= 19){
            if(n == 15 || n == 16)
                return n;
            else
                return 0;
        }
        return n;
    }

    public int blackJack(int a, int b) {
        if (a <= 0 || b <= 0)
            return 0;
        if (a > 21 && b > 21)
            return 0;

        if ((a > b) && (a <= 21))
            return a;
        else if(b <= 21)
            return b;
        else
            return a;
    }
}
