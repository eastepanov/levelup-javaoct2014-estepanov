package com.levelup.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * Created by denis_zavadsky on 12/6/14.
 */
public class DBManager {

    private static final String DB_URL="jdbc:mysql://localhost:3306/levelup";
    private static final String DB_USERNAME="root";
    private static final String DB_PASSWORD="root";
    private static final String DB_DRIVER_NAME="com.mysql.jdbc.Driver";

    private static DBManager instance;
    //private static Connection connection;

    private DBManager() {
        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    public static DBManager getInstance(){
        if (instance==null){
            instance = new DBManager();
        }
        return instance;
    }

    public Connection getConnection() throws SQLException {
        Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/levelup", "root", "root");
        return connection;
    }

//    public static Connection getManagedConnection() throws SQLException {
//        connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/levelup", "root", "root");
//        return connection;
//    }

//    public void setConnection(Connection connection) {
//        this.connection = connection;
//    }
}
