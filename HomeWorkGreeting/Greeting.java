package model;

/**
 * Created by SMULL on 22.10.2014.
 */
public interface Greeting {

    void greet();
    void greetWithName(String name);
}
