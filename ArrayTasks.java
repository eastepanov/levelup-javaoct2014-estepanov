package Exercises;

import java.util.Arrays;


public class ArrayTasks {

    public static String[] createArray (int N){
        if(N <= 0) {
            String[] arr = new String[0];
            return arr;
        }
        String[] arr = new String[N];
        for(int i = 0; i < N; ++i)
            arr[i] = Integer.toString(i);
        return arr;
    }

    public static boolean either (int [] arr){
        boolean flagTwo = false;
        boolean flagFour = false;
        for(int i = 0; i < arr.length - 1; i++){
            if(arr[i] == 2 && arr[i+1] == 2) {
                flagTwo = true;
            }
            if(arr[i] == 4 && arr[i+1] == 4)
                flagFour = true;
        }
        if(flagTwo == true && flagFour == true)
            return false;
        return true;
    }

    public static boolean has12 (int [] arr){
        for(int i = 0; i < arr.length - 1; ++i) {
            if (arr[i] == 1) {
                for (int j = i; j < arr.length; ++j)
                    if (arr[j] == 2) {
                        return true;
                    }
            }
        }
        return false;
    }

    public static boolean twoTwo (int [] arr){
        int sumOfOneInRow = 0, oneInRow = 0;
        int [] mas = new int[arr.length];
        for(int i = 0; i < arr.length; ++i) {
            mas[i] = 0;
            for (int j = i; j < arr.length; ++j)
                if (arr[i] == 7 && arr[j] == 7) {
                    mas[i] = 1;
                }
        }
        int key[] = new int[mas.length];
        for(int i = 0; i < mas.length; ++i) {

            if(mas[i] == 1) {
                sumOfOneInRow++;
            }
            if(i == mas.length-1)
                key[oneInRow] = sumOfOneInRow;
            else if((mas[i] == 0 && sumOfOneInRow != 0)){
                key[oneInRow++] = sumOfOneInRow;
                sumOfOneInRow = 0;
            }
        }
        for(int i = 0; i < key.length; i++) {
            if(key[i] == 1)
                return false;
        }
        return true;
    }

    public static int[] createArray2 (int start, int end){
        int variableLoop = 0;
        int [] arr = new int[end - start];
        for(int i = start; i < end; i++)
            arr[variableLoop++] = i;
        return arr;
    }

    public static int[] pre4 (int [] arr){
        int amountBefore4 = 0;
        if(arr.length == 0)
            return arr;
        for(int i = 0; i < arr.length; ++i) {
            if (arr[i] == 4)
                break;
            amountBefore4++;
        }
        int [] array = new int[amountBefore4];
        for(int i = 0; i < arr.length; ++i) {
            if (arr[i] == 4)
                return array;
            else
                array[i] = arr[i];
        }
        return array;
    }

    public static int[] zeroFront (int [] arr){
        int quantityZero = 0;
        int[] masWithZeroFront = new int[arr.length];

        for(int i = 0; i < arr.length; ++i)
            if(arr[i] == 0)
                quantityZero++;

        for(int i = 0; i < quantityZero; ++i)
            masWithZeroFront[i] = 0;
        int counterArr = quantityZero;
        for(int i = 0; i < arr.length; ++i)
            if(arr[i] != 0)
                masWithZeroFront[counterArr++] = arr[i];
        return masWithZeroFront;
    }

    public static int [] evenOdd (int [] arr){
        int quantityOf2 = 0;
        int quantityOfEnd2 = 0;
        int[] masOdd = new int[arr.length];
        for(int i = 0; i < arr.length; ++i)
            if(arr[i] % 2 == 0)
                quantityOf2++;

        int[] masOnlyOdd = new int[quantityOf2];
        int counterMasOdd = 0;

        for(int j = 0; j < arr.length; ++j)
            if(arr[j] % 2 == 0) {
                masOnlyOdd[counterMasOdd++] = arr[j];
            }
        Arrays.sort(masOnlyOdd);

        for(int i = 0; i < masOnlyOdd.length; ++i) {
            masOdd[i] = masOnlyOdd[i];
            quantityOfEnd2 = i;
        }
        for(int i = 0; i < arr.length; ++i)
            if(arr[i] % 2 != 0)
                masOdd[++quantityOfEnd2] = arr[i];
        return masOdd;
    }

    public static int sum67 (int[] arr){
        int sum = 0;
        int[] arrResult = arr;

        for(int i = 0; i < arr.length; ++i) {
            if (arr[i] == 6) {
                for (int j = i; j != arr.length - 1; ++j) {
                    if(arr[j] == 7 && arr[j + 1] != 7){
                        arrResult[j++] = 0;
                        break;
                    }
                    arrResult[j] = 0;
                }
            }
        }
        for(int i = 0; i < arrResult.length; ++i) {
            sum += arrResult[i];
        }
        return  sum;
    }

    public static boolean tripleUp (int [] arr){

        for(int i = 0; i < arr.length-2; ++i){
            if((arr[i+1] == (arr[i] += 1)) && (arr[i+2] == (arr[i+1] += 1)))
                return true;
        }
        return false;
    }

    public static void printArrayTasks(){
        //Task#1
     String[] myArray = createArray(8);
     for( String str : myArray)
        System.out.print(str + " ");

        //Task#2
     System.out.println("\n==========================");
     int[] arr = {1,2,2,7,4,2,2};
     System.out.println(either(arr));

        //Task#3
     System.out.println("\n==========================");
     int[] arrHas12 = {1,4,3,4,5,6};
     System.out.println(has12(arrHas12));

        //Task#4
     System.out.println("\n==========================");
     int[] arrTwoTwo = {4,7,7,7,7,7,2,3,7,7,7,3,5,3,1};
     System.out.println(twoTwo(arrTwoTwo));

        //Task#5
     System.out.println("\n==========================");
     int[] arrCreateArray2 = createArray2(7,14);
     for (int i : arrCreateArray2)
         System.out.print(i + " ");

        //Task#6
     System.out.println("\n==========================");
     int[] arrPre4 = {1,4,4};
     int[] arrResult = pre4(arrPre4);
     for(int i : arrResult)
         System.out.print(i + " ");

        //Task#7
     System.out.println("\n==========================");
     int[] arrZeroFront = {1,0,0,1,3,5,3,0,0,0,3,0};
     int[] arrResultZeroFront = zeroFront(arrZeroFront);
     for(int i : arrResultZeroFront)
         System.out.print(i + " ");

        //Task#8
     System.out.println("\n==========================");
     int[] arrEvenOdd = {1,0,1,0,0,2,0,6,7,2,4,3,5,0,5,2};
     int[] arrResultEvenOdd = evenOdd(arrEvenOdd);
     for(int i : arrResultEvenOdd)
         System.out.print(i + " ");


        //Task#9
     System.out.println("\n==========================");
     int[] arrSum67 = {1,2,2,6,99,99,7,7,7,2,3,2,4,5,6,7,1,2,6,76,432,12,41,21,7,7,7,7,1,7};
     System.out.println(sum67(arrSum67));

        //Task#10
     System.out.println("\n==========================");
     int[] arrTripleUp = {23,24,25,1,2,5,67,83,1,4,2};
     System.out.println(tripleUp(arrTripleUp));

    }

}
