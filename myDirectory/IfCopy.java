package com.levelUp.myDirectory;

import java.io.File;

/**
 * Created by SMULL on 15.11.2014.
 */
public interface IfCopy {
    void copyDirectory(File src, File dst);
}
