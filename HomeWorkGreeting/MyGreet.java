package model;


public class MyGreet implements EnglishGreeting{

    @Override
    public void greet() {
        System.out.println("Hello!");
    }

    @Override
    public void greetWithName(String name) {
        System.out.println("Hello," + name);
    }

    public class Inner implements FrenchGreeting{

            @Override
            public void greet() {
                System.out.println("Salut!");
            }

            @Override
            public void greetWithName(String name) {
                System.out.println("Salut, " + name);
            }
        }

}
