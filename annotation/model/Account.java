package com.levelup.model;

/**
 * Created by denis_zavadsky on 12/6/14.
 */
public class Account {

    public static final String INSERT_ACCOUNT = "INSERT INTO Account(username, password, actorId) VALUES(?,?,?)";

    private Long id;
    private String username;
    private String password;
    private Long actorId;

    private Actor actor;

    public Account() {
    }

    public Account(Long id, String username, String password, Long actorId) {
        this.id = id;
        this.username = username;
        this.password = password;
        this.actorId = actorId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Long getActorId() {
        return actorId;
    }

    public void setActorId(Long actorId) {
        this.actorId = actorId;
    }

    public Actor getActor() {
        return actor;
    }

    public void setActor(Actor actor) {
        this.actor = actor;
    }
}
