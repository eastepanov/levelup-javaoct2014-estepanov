package com.model.mylist;

/**
 * Created by SMULL on 31.10.2014.
 */
public class MyMain {
    public static void main(String[] args) {
        MyList myList = new MyList();
        myList.add(12);
        myList.add(45);
        myList.add(566);
        myList.add(4);
        myList.add(345);


        System.out.print(myList.getHead() + " ");
        System.out.println();
        System.out.println("================================");
        System.out.print(myList.getTail() + " ");
        System.out.println();
        System.out.println("================================");
        //System.out.println(myList.getSize());
        for(int i = 0; i < myList.getSize(); i++)
            System.out.print(myList.get(i) + " ");
        System.out.println();
        System.out.println("================================");
        for(Object o : myList)
            System.out.print(o + " ");

        myList.add(23456,3);
        myList.add(999,5);
        System.out.println();
        System.out.println("==================");
        myList.remove();
        for(Object o : myList)
            System.out.print(o + " ");


        System.out.println();
        System.out.println("==================");
        try {
            myList.remove(4);
            for (Object o : myList)
                System.out.print(o + " ");
        }catch (Exception e){
            e.getMessage();
        }

        System.out.println();
        boolean reval = myList.contains(999);
        System.out.println(reval);
        reval = myList.contains(1000);
        System.out.println(reval);


        System.out.println("Index = " + myList.indexOf(999));


        MyList myList1 = new MyList();
        myList1.addInRow(999);
        myList1.addInRow(998);
        myList1.addInRow(43);
        myList1.addInRow(3);
        myList1.addInRow(343434);
        for (Object o : myList1)
            System.out.print(o + " ");

        System.out.println();
        myList.addInRow(998);
        myList.addInRow(3);
        for (Object o : myList)
            System.out.print(o + " ");
    }



}
