package com.model.mylist;

/**
 * Created by SMULL on 31.10.2014.
 */
public class Node {

    protected Node next;
    private int data;

    public Node(Integer data) {
        this.data = data;
    }

    public Node() {

    }

    public int getData() {
        return data;
    }

    public void setData(int data) {
        this.data = data;
    }
}
