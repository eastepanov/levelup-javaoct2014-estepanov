package my.level.up.lab1;


public abstract class Animal {
    private int quantityOfLegs;
    private int weight;
    private String typeOfAnimals;
    protected char meal;


    public Animal() {

    }

    public Animal(String typeOfAnimals, int quantityOfLegs, int weight, char meal) {
        this.typeOfAnimals = typeOfAnimals;
        this.weight = weight;
        this.quantityOfLegs = quantityOfLegs;
        this.meal = meal;
    }

    public char getMeal() {
        return meal;
    }

    public void setMeal(char meal) {
        this.meal = meal;
    }

    public int getQuantityOfLegs() {
        return quantityOfLegs;
    }

    public void setQuantityOfLegs(int quantityOfLegs) {
        this.quantityOfLegs = quantityOfLegs;
    }

    public String getTypeOfAnimals() {
        return typeOfAnimals;
    }

    public void setTypeOfAnimals(String typeOfAnimals) {
        this.typeOfAnimals = typeOfAnimals;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public abstract void eat(char meal);

    public abstract void drink();

    public void sleep() {
        System.out.println("I`m sleeping!");
    }

    public void go() {
        System.out.println("I`m going now!");
    }

    @Override
    public String toString() {
        return "\t\t\t-=Animal=-\nType: " + typeOfAnimals +
                "\nQuantityOfLegs = " + quantityOfLegs +
                "\nWeight = " + weight;
    }
}
