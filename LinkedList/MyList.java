package com.model.mylist;

import java.util.Iterator;

/**
 * Created by SMULL on 31.10.2014.
 */
public class MyList implements Iterable{
    private Node head;
    private int size = 0;

    public void add(int data){
        if(size == 0){
            head = new Node(data);
        }else {
            Node temp = head;
            for(int i = 0; i < size - 1; i++){
               temp = temp.next;
            }
            temp.next = new Node(data);
        }
        size++;
    }

    public void add(int data, int index){
        if(index == 0)
            head = new Node(data);
        else {
            Node newInverterNode = new Node(data);
            Node newNode = head;
            for (int i = 0; i < index - 2; ++i) {
                newNode = newNode.next;
            }
            newInverterNode.next = newNode.next;
            newNode.next = newInverterNode;
        }
        size++;
    }

    public int getSize() {
        return size;
    }

    public Integer getTail(){
        Node newNode = head;
        for(int i = 0; i < size - 1; ++i)
            newNode = newNode.next;
        return newNode.getData();
    }

    public Integer getHead(){
        return head.getData();
    }

    public Integer get(int index){
        Node newNode = head;

        for(int i = 0; i < index; i++){
           newNode = newNode.next;
        }
        return newNode.getData();
    }

    public void remove(){
        Node newNode = head;
        for(int i = 0; i < size - 1; i++){
            newNode = newNode.next;
        }
        newNode.next = null;
        size--;
    }


    public void addInRow(int data){
        if(size == 0)
            head = new Node(data);
        else if(head.getData() > data){
            Node newInsertedNode = new Node(data);
            newInsertedNode.next = head;
            head = newInsertedNode;
        }
        else{
            Node newNode = head;
            Node newInsertedNode = new Node(data);
            for(int i = 0; i < size - 1 ; ++i){
                if(data < newNode.next.getData()) {
                    break;
                }
                newNode = newNode.next;
            }
            newInsertedNode.next = newNode.next;
            newNode.next = newInsertedNode;
        }
        size++;
    }

    public Integer indexOf(int data){
        Node newNode = head;
        int index;
        for(int i = 0; i < size - 1; ++i){
            if(newNode.getData() == data)
                return i;
            else
                newNode = newNode.next;
        }
        return 0;
    }

    public boolean contains(int data){
        Node newNode = head;
        boolean flag;
        for(int i = 0; i < size - 1; ++i){
            if(newNode.getData() == data)
                return true;
            else
                newNode = newNode.next;
        }
        return false;
    }

    public void remove(int index) throws Exception {
        if(index > size)
            throw new Exception("OutOfRange");
        Node newNode = head;
        for(int i = 0; i < index - 2; ++i){
            newNode = newNode.next;
        }
        newNode.next = newNode.next.next;
        size--;

    }

    @Override
    public Iterator iterator() {
        return new MyListIterator();
    }

    private class MyListIterator implements Iterator {

        private int currentPosition;
        @Override
        public boolean hasNext() {
            return currentPosition < size;
        }

        @Override
        public Object next() {
            return MyList.this.get(currentPosition++);
        }

        @Override
        public void remove() {

        }
    }
}
