package com.levelUp.model;

import sun.text.normalizer.UTF16;
import sun.text.normalizer.UnicodeSet;

import java.io.*;
import java.util.ArrayList;
import java.util.List;


public class MyClass {

    static final String path = "C:\\temp.txt";
    static final String result = "C:\\result.txt";
    static List<String> myList = new ArrayList<String>();

    public void readFile(String path) {
        File myFile = new File(path);

        if (!myFile.exists() || !myFile.isFile() || !myFile.canRead()) {
            System.out.println("Error: File temp.txt doesn`t exist!");
            return;
        } else {
            try {
                BufferedReader in = new BufferedReader(new FileReader(myFile),100);
                String strForRead;
                while ((strForRead = in.readLine()) != null) {
                   if( strForRead.toLowerCase().contains("java")){
                        myList.add(strForRead);
                    }
                }
                in.close();
            } catch (IOException e) {
                e.getMessage();
            }
        }
    }

    public void writeResult(String path){
        File myFileResult = new File(path);
        try {
            if (!myFileResult.exists()) {
                myFileResult.createNewFile();
            }
            FileWriter myFileWriter = new FileWriter(myFileResult);
            for(String str : myList){
                myFileWriter.write(str + "\r\n");
                myFileWriter.flush();
            }
            myFileWriter.close();
        }catch (IOException e) {
            e.getMessage();
        }
    }

    public void printFile(String path) {
        File myFile = new File(path);

        if (!myFile.exists() || !myFile.isFile() || !myFile.canRead()) {
            System.out.println("Error: File temp.txt doesn`t exist!");
            return;
        } else {
            try {
                FileReader myReader = new FileReader(myFile);
                BufferedReader in = new BufferedReader(myReader);
                String strForRead;
                while ((strForRead = in.readLine()) != null) {
                    System.out.println(strForRead);
                }
            }catch (IOException e){
                e.getMessage();
            }
        }
    }

    public static void main(String[] args) {

        MyClass myClass = new MyClass();
        myClass.readFile(path);
        myClass.writeResult(result);
        myClass.printFile(result);
    }

}
