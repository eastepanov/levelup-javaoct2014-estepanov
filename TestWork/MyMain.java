package com.levelUp;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Objects;

/**
 * Created by user on 25.11.2014.
 */
public class MyMain {
    static final String ID1 = "123456";
    static final String ID2 = "987654";
    static final String ID3 = "159741";
    static final String ID4 = "357829";
    static final String ID5 = "365417";
    static final String ID6 = "951475";
    static HashMap<String,LinkedList<Part>> store = new HashMap<String, LinkedList<Part>>();


    public static void main(String[] args) {

        initStore(store);

        Toyota toyota = (Toyota) initAnotations("Toyota");
        Honda honda = (Honda) initAnotations("Honda");
        Bmw bmw = (Bmw) initAnotations("Bmw");


        System.out.println(toyota.toString());
        System.out.println(honda.toString());
        System.out.println(bmw.toString());

    }

    public static Object initAnotations(String classMy) {
        String tempStr = "com.levelUp." + classMy;
        Class classCar = null;
        Object classObj = null;
        try {
            classCar = Class.forName(tempStr);

            classObj = classCar.newInstance();

            //Toyota
            Field[] fieldsToyota = classCar.getDeclaredFields();
            for (Field f : fieldsToyota) {
                Applicable ann = (Applicable) f.getAnnotation(Applicable.class);
                int annLength = ann.identity().length;
                for (String id : ann.identity()) {
                    if (id != null || id != "") {
                        if (store.containsKey(id)) {
                            LinkedList<Part> temp = store.get(id);
                            if (!temp.isEmpty()) {
                                f.setAccessible(true);
                                f.set(classObj, temp.poll());
                                break;
                            } else {
                                continue;
                            }


                        } else {
                            System.out.println("Detail" + id + " is absent");
                        }
                        annLength--;
                    }

                }
            }
            //System.out.println(classObj);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }

        return classObj;
    }

    public static void initStore(HashMap<String,LinkedList<Part>> store){
        LinkedList<Part> listDetail = new LinkedList<Part>();
        //ID1
        listDetail.add(new Part("@@@1"));
        listDetail.add(new Part("@@@2"));
        listDetail.add(new Part("@@@3"));
        listDetail.add(new Part("@@@4"));

        store.put(ID1,listDetail);

        //ID2
        listDetail = new LinkedList<Part>();
        listDetail.add(new Part("@@@1"));
        listDetail.add(new Part("@@@2"));
        listDetail.add(new Part("@@@3"));
        listDetail.add(new Part("@@@4"));

        store.put(ID2,listDetail);

        //ID3
        listDetail = new LinkedList<Part>();
        listDetail.add(new Part("@@@1"));
        listDetail.add(new Part("@@@2"));
        listDetail.add(new Part("@@@3"));
        listDetail.add(new Part("@@@4"));

        store.put(ID3,listDetail);

        //ID4
        listDetail = new LinkedList<Part>();
        listDetail.add(new Part("@@@1"));
        listDetail.add(new Part("@@@2"));
        listDetail.add(new Part("@@@3"));
        listDetail.add(new Part("@@@4"));

        store.put(ID4,listDetail);

        //ID5
        listDetail = new LinkedList<Part>();
        listDetail.add(new Part("@@@1"));
        listDetail.add(new Part("@@@2"));
        listDetail.add(new Part("@@@3"));
        listDetail.add(new Part("@@@4"));

        store.put(ID5,listDetail);

        //ID6
        listDetail = new LinkedList<Part>();
        listDetail.add(new Part("@@@1"));
        listDetail.add(new Part("@@@2"));
        listDetail.add(new Part("@@@3"));
        listDetail.add(new Part("@@@4"));

        store.put(ID6,listDetail);
    }
}
