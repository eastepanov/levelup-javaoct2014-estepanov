package com.levelUp.myDirectory;


import java.io.*;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Stack;


public class MyDirectory implements IfCopy {

    private File src;
    private File dst;
    private  boolean flagOfHowToCopy;
    private String checkedString = "Image";
    private String strDestination;
    static final String stick = "\\";
    private List<String> list = new ArrayList<>();

    public MyDirectory(boolean flagOfHowToCopy) {
        this.flagOfHowToCopy = flagOfHowToCopy;

    }

    public void setFlagOfHowToCopy(boolean flagOfHowToCopy) {
        this.flagOfHowToCopy = flagOfHowToCopy;
    }

    @Override
    public void copyDirectory(File src, File dst) {
       this.src = src;
       this.dst = dst;
       strDestination = new String(dst.getPath());
       howToCopy(flagOfHowToCopy);
    }

    private void howToCopy(boolean flag){
        if(flag == true){
            copyDirectoryWithHierarchy(src,dst);
        }else{
            copyDirectoryWithoutHierarchy(src,dst);
        }
    }

    private void copyDirectoryWithHierarchy(File src, File dst) {
        if(src.isDirectory()){
            if(!dst.exists()){
                dst.mkdir();
                System.out.println("Directory copy from " + src + " to " + dst);
            }
            String[] list = src.list();
            for(String file : list){
                File srcFile = new File(src,file);
                File dstFile = new File(dst,file);
                copyDirectory(srcFile,dstFile);
            }
        }else{
            recordFiles(src,dst);
        }

    }

    private void recordFiles(File src, File dst){
        try {
            InputStream in = new FileInputStream(src);
            OutputStream out = new FileOutputStream(dst);

            byte[] buffer = new byte[1024];
            int length;
            while((length = in.read(buffer)) > 0){
               out.write(buffer,0,length);
             }
            in.close();
            out.close();
        } catch (FileNotFoundException e) {
             e.printStackTrace();
        }catch (IOException e){
             e.printStackTrace();
        }
    }

    private void copyDirectoryWithoutHierarchy(File src, File dst) {
        Stack<File> stackOnlyFiles = new Stack<File>();
        Stack<File> stackForLoop = new Stack<File>();

        stackForLoop.push(src);
        while(!stackForLoop.isEmpty()) {
            File child = stackForLoop.pop();
            if (child.isDirectory()) {
                for(File f : child.listFiles()) stackForLoop.push( f );
            } else if (child.isFile()) {
                stackOnlyFiles.push(child);
            }
        }
        String strE = null;
        while(!stackOnlyFiles.isEmpty()) {
            File child = stackOnlyFiles.pop();
            String sd = checkedPath(child.getName());
            strE = clay(sd);
            list.add(child.getName());

            RecordByFilter(child,strE);
        }


   }
   private String checkedPath(String str){
        String newString = null;
        int i = 2;
        for(String s : list){
            if(s.equals(str)){
                newString = addNextNumberOfNameOfFile(str,i);
                for(String st : list){
                    if(newString.equals(st)){
                        i++;
                        newString = addNextNumberOfNameOfFile(str, i);
                    }
                }
                list.add(newString);
                return newString;
            }
        }
        return str;
    }

    private String addNextNumberOfNameOfFile(String str, int i){
        int index = str.indexOf(".");
        String NamePastDot = str.substring(index);
        String NameBeforeDot = str.substring(0,index);
        return (NameBeforeDot + "(" + Integer.toString(i) + ")" + NamePastDot);
    }

    private String clay(String str){
        return new String(strDestination + stick + str);
    }

    private void RecordByFilter(File file,String str){
        if(checkedFile(file.getName())){
            recordFiles(file,new File(str));
        }
    }

    private boolean checkedFile(String str){
        return str.toLowerCase().indexOf(checkedString.toLowerCase()) > -1;
    }

/////////////////////////////////////////////////////////////////////////////////////////////



    public static void main(String[] args) {
        IfCopy ifCopy = new MyDirectory(false);

        File source = new File("C:\\MyFirst");
        File destination =  new File("C:\\MyTempRes");


        if(!source.exists()){
            System.out.println("Directory doesn`t exist!");
            System.exit(0);
        }else{
            if(!destination.exists())
                destination.mkdir();
        }

        ifCopy.copyDirectory(source, destination);
        System.out.println("Done");
    }
}
