package my.level.up.lab1;


public class Bird extends Animal {
    private int wings;
    private char beak;
    private int claws;

    public Bird() {

    }

    public Bird(String typeOfAnimals, int quantityOfLegs, int weight, char meal, int wings, char beak, int claws) {
        super(typeOfAnimals, quantityOfLegs, weight, meal);
        this.wings = wings;
        this.beak = beak;
        this.claws = claws;
    }


    public int getClaws() {
        return claws;
    }

    public void setClaws(int claws) {
        this.claws = claws;
    }

    public int getBeak() {
        return beak;
    }

    public void setBeak(char beak) {
        this.beak = beak;
    }

    public int getWings() {
        return wings;
    }

    public void setWings(int wings) {
        this.wings = wings;
    }

    @Override
    public void eat(char meal) {        //override
        System.out.println(super.getTypeOfAnimals() +
                ": I`m eating " + meal + "by means of" + beak);
    }

    @Override
    public void drink() {
        System.out.println(super.getTypeOfAnimals() +
                ": I`m drinking now!");
    }

    @Override
    public String toString() {
        return super.toString() + "\nWings = " + wings +
                "\nBeak = " + beak +
                "\nClaws = " + claws;
    }

    public void fly() {
        System.out.println(super.getTypeOfAnimals() +
                ": I`m flying now!");
    }

    public void hunt(char beak, int claws) {
        System.out.println(super.getTypeOfAnimals() +
                ": I`m hunting by means of " + beak + " and " + claws + "claws");
    }

    public void setWeight(double weight) { //overloading
        if(weight == 0){
            return;
        }
        System.out.println("Bird`s weight is " + weight);
    }
}
