package com.levelup.dao;

import com.levelup.model.Column;
import com.levelup.model.Table;
import com.levelup.model.User;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.util.LinkedList;

/**
 * Created by user on 09.12.2014.
 */
public class DBEngine {



    public DBEngine() {
    }

    public static String createSelectQuery(Class clazz){
        String resultQuery = "";
        Object classObj = null;
        try {
            classObj = clazz.newInstance();
            Field[] fields = clazz.getDeclaredFields();
            int length = fields.length;
            for (Field f : fields) {
                length--;
                Column col = f.getAnnotation(Column.class);
                resultQuery += col.nameColumn();
                if (length != 0) {
                    resultQuery += ", ";
                }
            }
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }

        String str = "SELECT " + resultQuery + " FROM " + ((Table) clazz.getAnnotation(Table.class)).nameTable() + " WHERE (id = ?)";
        return str;
    }


    public  static  String createInsertQuery(Class clazz) {
        String resultQuery = "INSERT INTO " + ((Table) clazz.getAnnotation(Table.class)).nameTable() + "(";
        Object classObj = null;
        try {
            classObj = clazz.newInstance();
            Field[] fields = clazz.getDeclaredFields();
            if(fields[0].isAnnotationPresent(Column.class) && fields[0].getAnnotation(Column.class).ind()) {
                int length = fields.length;
                for (Field f : fields) {
                  length--;
                  if(!f.getAnnotation(Column.class).ind()) {
                      Column col = f.getAnnotation(Column.class);
                      resultQuery += col.nameColumn();
                      if (length != 0) {
                          resultQuery += ", ";
                      }
                  }
               }
            }
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        resultQuery += " ) VALUES (?,?,?)";
        return resultQuery;
    }

}
