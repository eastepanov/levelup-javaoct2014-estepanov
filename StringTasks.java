package Exercises;

/**
 * Created by SMULL on 12.10.2014.
 */
public class StringTasks {

    public static String wrapWord(String wrap, String str) {
        return wrap.substring(0, 2) + str + wrap.substring(2);
    }

    public static String firstHalf(String str) {
        return str.substring(0, str.length() / 2);
    }

    public static String nonStart(String first, String second) {

        if (first.length() == 1)
            return first + second.substring(1);
        if (second.length() == 1)
            return first.substring(1) + second;
        return first.substring(1) + second.substring(1);
    }

    public static boolean hasBad(String str) {
        if (str.startsWith("bad", 0) || str.startsWith("bad", 1))
            return true;
        return false;
    }

    public static String withoutX(String str) {
        String result = "";
        if (str.charAt(0) == 'x' && str.charAt(str.length() - 1) == 'x')
            return result = str.substring(1, str.length() - 1);
        else if (str.startsWith("x"))
            return result = str.substring(1);
        else if (str.endsWith("x"))
            return result = str.substring(0, str.length() - 1);
        else
            return str;
    }

    public static String makeTags(String first, String second) {
        String tagBegin = '<' + first + '>';
        String tagEnd = "</" + first + '>';
        return tagBegin + second + tagEnd;
    }

    public static String atFirst(String str) {
        if (str.length() >= 2) {
            return str.substring(0, 2);
        } else if (str.length() == 1)
            return str += '@';
        else
            return str += "@@";
    }

    public static String comboString(String a, String b) {
        if (a.length() == b.length())
            return "";
        if (a == "" || b == "")
            return "";
        else if (a.length() < b.length())
            return a + b + a;
        else
            return b + a + b;
    }

    public static String right2(String str) {
        char beforeLastLetter;
        char lastLetter;
        if (str.length() < 2)
            return "";
        else {
            beforeLastLetter = str.charAt(str.length() - 2);
            lastLetter = str.charAt(str.length() - 1);
            return lastLetter + "" + beforeLastLetter + "" + str.substring(0, str.length() - 2);
        }
    }

    public static String minCat(String first, String second) {
        if (first.length() == second.length())
            return first + second;
        if (first.length() < second.length() && first.length() != 0) {
            return first + second.substring((second.length() - first.length()));
        }
        if(first.length() > second.length() && second.length() != 0) {
            return first.substring((first.length() - second.length())) + second;
        }
        return first + second;
    }

    public static void printStringTasks(){
        //Task#1
        String str = "Hello";
        System.out.println(StringTasks.wrapWord("[[]]", str));

        System.out.println("=================================");
        //Task#2
        System.out.println(StringTasks.firstHalf("HelloThere"));

        System.out.println("=================================");
        //Task#3
        System.out.println(StringTasks.nonStart("H", "There"));

        System.out.println("=================================");
        //Task#4
        System.out.println(StringTasks.hasBad("xbadxx"));

        System.out.println("=================================");
        //Task#5
        System.out.println(StringTasks.withoutX("xBaddsdsix"));

        System.out.println("=================================");
        //Task#6
        System.out.println(StringTasks.makeTags("cite", "Hello"));

        System.out.println("=================================");
        //Task#7
        System.out.println(StringTasks.atFirst("Hello"));

        System.out.println("=================================");
        //Task#8
        System.out.println(StringTasks.comboString("Hi", "Hello"));

        System.out.println("=================================");
        //Task#9
        System.out.println(StringTasks.right2("red_ _"));

        System.out.println("=================================");
        //Task#10
        System.out.println(StringTasks.minCat("tr", "Hello"));

    }
}
