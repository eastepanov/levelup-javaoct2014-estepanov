package com.levelUp.homework;

import java.io.File;
import java.io.IOException;
import java.util.*;


public class Task {

    private TreeMap<String,MyFile> myFileTreeMap = new TreeMap<>();
    private TreeMap<String,MyFolder> myFolderTreeMap = new TreeMap<>();
    private File myDirectory;

    public Task() {
    }

    public Task(String path){
        myDirectory = new File(path);
        read(myDirectory);
        printStruct();

    }

    private void read(File way) {
        if(way.isDirectory()) {
            File[] all = way.listFiles();
            for(int i = 0; i < all.length; ++i){
                if(all[i].isDirectory()){
                    MyFolder myFolder = new MyFolder(all[i]);
                    myFolderTreeMap.put(all[i].getName(), myFolder);
                    read(all[i]);
                }else  if(all[i].isFile()){
                    MyFile myFile = new MyFile(all[i]);
                    myFileTreeMap.put(all[i].getName(), myFile);

                }
            }
        }
    }


    private void max(){
        Iterator entries = myFolderTreeMap.entrySet().iterator();
        while (entries.hasNext()) {
            Map.Entry thisEntryMy = (Map.Entry) entries.next();
            if(MyFolder.myLength < ((String)thisEntryMy.getKey()).length())
            ((MyFolder) (thisEntryMy.getValue())).
                    setMyLength(((String) (thisEntryMy.getKey())).length());

        }
        System.out.println();
        entries = myFileTreeMap.entrySet().iterator();
        while (entries.hasNext()) {
            Map.Entry thisEntryMy = (Map.Entry) entries.next();
            if(MyFolder.myLength < ((String)thisEntryMy.getKey()).length())
                MyFolder.myLength =  ((String)(thisEntryMy.getKey())).length();

        }

    }

    public void printStruct(){
        max();
        Iterator entries = myFolderTreeMap.entrySet().iterator();
        while (entries.hasNext()) {
            Map.Entry thisEntry = (Map.Entry) entries.next();
            System.out.println(thisEntry.getValue());
        }

        max();
        entries = myFileTreeMap.entrySet().iterator();
        while (entries.hasNext()) {
            Map.Entry thisEntry = (Map.Entry) entries.next();
            System.out.println(thisEntry.getValue());
        }
    }


    public static void main(String[] args) {

        System.out.println("Enter the path to the directory: ");
        Scanner in = new Scanner(System.in);

        String path = in.nextLine();

        if(new File(path).isDirectory()) {
             Task myTask = new Task(path);
        }else
            System.out.println("this is not directory!");

    }

}
