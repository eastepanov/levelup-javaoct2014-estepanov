package com.model;

import java.util.Comparator;

/**
 * Created by SMULL on 05.11.2014.
 */
public interface Compared  extends Comparator<Integer>{
    @Override
    int compare(Integer o1, Integer o2);
}
