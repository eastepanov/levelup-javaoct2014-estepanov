package com.levelUp.homework;

import java.io.File;
import java.util.Comparator;

/**
 * Created by SMULL on 19.11.2014.
 */
public class MyFolder implements Comparator<String> {


    static final String folder = "FOLDER";
    private String strSpace;
    private File nameFolder;

    public static int myLength = 15;

    public void setMyLength(int myLength) {
        this.myLength = myLength;
    }

    public int getMyLength() {
        return myLength;
    }

    public MyFolder() {
    }

    public MyFolder(File nameFile) {
        this.nameFolder = nameFile;

    }



    @Override
    public int compare(String o1, String o2) {
        int k = o1.substring(o1.indexOf(" "))
                .compareTo(o2.substring(o2.indexOf(" ")));
        if(k == 0) {
            return o1.compareTo(o2);
        }
        else {
            return k;
        }
    }


    @Override
    public String toString() {
        if(myLength == nameFolder.getName().length())
            return  nameFolder.getName() + "\t" + folder;
        else {
            strSpace = "";
            for (int i = 0; i < (myLength - nameFolder.getName().length()); i++) {
                strSpace += " ";
            }
            return nameFolder.getName() + strSpace + "\t" + folder;
        }
    }
}
