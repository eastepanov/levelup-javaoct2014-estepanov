package com.levelup.model;

import java.util.Date;

/**
 * Created by denis_zavadsky on 12/6/14.
 */

@Table(nameTable = "User")
public class User {

    @Column(nameColumn = "id",ind = true)
    private Long id;
    @Column(nameColumn = "firstname")
    private String firstName;
    @Column(nameColumn = "lastname")
    private String lastName;
    @Column(nameColumn = "birthdate")
    private Date birthDate;

    public User() {
    }

    public User(Long id, String firstName, String lastName, Date birthDate) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.birthDate = birthDate;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }
}
