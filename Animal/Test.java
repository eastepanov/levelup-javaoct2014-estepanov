package my.level.up.lab1;

public class Test {
    public static void main(String[] args) {

        Animal tiger = new Tiger("Tiger", 4, 250, 'm', 2, 10);
        Tiger whiteTiger = new Tiger("WhiteTiger", 4, 250, 'm', 2, 10);

        Animal bird = new Bird("Bird", 2, 2, 'w', 2, 'b', 8);
        Bird eagle = new Bird("Eagle", 2, 2, 'w', 2, 'b', 8);

        Animal elephant = new Elephant("Elephant", 4, 12000, 'g', 2, 'h');
        Elephant loxodonta = new Elephant("Loxodonta", 4, 10000, 'g', 2, 'h');

        System.out.println(tiger);
        System.out.println();
        System.out.println(bird);
        System.out.println();
        System.out.println(elephant);


        System.out.println("=======================================");

        //Tiger
        tiger.eat('m');
        tiger.drink();
        tiger.go();
        tiger.sleep();

        System.out.println("=======================================");

        //WhiteTiger
        whiteTiger.climbOnTree(10);
        whiteTiger.hunt(2, 10);

        System.out.println("=======================================");

        //Bird
        bird.eat('m');
        bird.drink();
        bird.go();
        bird.sleep();

        System.out.println("=======================================");

        //Eagle
        eagle.fly();
        eagle.hunt('b', 8);


        System.out.println("=======================================");

        elephant.eat('g');
        elephant.drink();
        elephant.go();
        elephant.sleep();

        System.out.println("=======================================");

        loxodonta.defend(2);
        loxodonta.takeShower('t');

    }
}
