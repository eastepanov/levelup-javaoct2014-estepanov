package com.levelUp;

/**
 * Created by user on 25.11.2014.
 */
public class Honda {

    @Applicable(identity = {"987654","123456","365417"})
    private Part part1;
    @Applicable(identity = {"123456","987654"})
    private Part part2;
    @Applicable(identity = {"357829","951475"})
    private Part part3;


    public Honda() {
        part1 = new Part();
        part2 = new Part();
        part3 = new Part();
    }

    public Honda(Part part1, Part part2, Part part3) {
        this.part1 = part1;
        this.part2 = part2;
        this.part3 = part3;
    }

    public Part getPart1() {
        return part1;
    }

    public void setPart1(Part part1) {
        this.part1 = part1;
    }

    public Part getPart2() {
        return part2;
    }

    public void setPart2(Part part2) {
        this.part2 = part2;
    }

    public Part getPart3() {
        return part3;
    }

    public void setPart3(Part part3) {
        this.part3 = part3;
    }

    @Override
    public String toString() {
        return "Honda:" + System.lineSeparator() +
                " part1 = " + part1.getIdNumber() + System.lineSeparator() +
                " part2 = " + part2.getIdNumber() +  System.lineSeparator() +
                " part3 = " + part3.getIdNumber() +  System.lineSeparator();
    }
}
