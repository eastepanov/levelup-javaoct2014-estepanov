package com.levelup.dao;

import com.levelup.model.Account;
import com.levelup.model.Actor;
import com.levelup.model.User;

import java.sql.*;
import java.util.Date;

/**
 * Created by denis_zavadsky on 12/6/14.
 */
public class AuthController {

    private final static String SELECT_USER_QUERY = "SELECT acc.id AS account_Id, acc.username, acc.password, acc.actorId, act.id AS actor_Id, act.role, act.userId,\n" +
            "u.id AS user_Id, u.firstName, u.lastName, u.birthDate \n" +
            "FROM Account acc JOIN Actor AS act ON (acc.actorId = act.id)\n" +
            "JOIN USER AS u ON (u.id = act.userId) WHERE acc.username = ?";
    private DBManager dbManager = DBManager.getInstance();
    private Connection connection;
    private PreparedStatement accountInsertStatement;
    private PreparedStatement actorInsertStatement;
    private PreparedStatement userInsertStatement;

    private PreparedStatement userSelectStatement;

    public AuthController() {
        try {
            connection = dbManager.getConnection();
            //accountInsertStatement = connection.prepareStatement(Account.INSERT_ACCOUNT, Statement.RETURN_GENERATED_KEYS);
            //actorInsertStatement = connection.prepareStatement(Actor.INSERT_ACTOR, Statement.RETURN_GENERATED_KEYS);
            userInsertStatement = connection.prepareStatement(DBEngine.createInsertQuery(User.class), Statement.RETURN_GENERATED_KEYS);
            userSelectStatement = connection.prepareStatement(DBEngine.createSelectQuery(User.class));
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public Account registerUser(String username, String password, String firstName, String lastName,
                                Date birthDate){
        Account account = new Account();
        account.setUsername(username);
        account.setPassword(password);

        Actor actor = new Actor();
        actor.setRole("USER");

        User user = new User();
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setBirthDate(birthDate);

        try {
            Long userId = saveUser(user);
            actor.setUserId(userId);
            Long actorId = saveActor(actor);
            account.setActorId(actorId);
            Long accountId = saveAccount(account);
            account.setId(accountId);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return  account;
    }

    public Account loginUser(String username, String password){
        Account account = null;
        boolean found = false;
        try {
            userSelectStatement.setString(1, username);
            ResultSet rs = userSelectStatement.executeQuery();


            if (rs.next()){
                User user = new User(rs.getLong("user_id"), rs.getString("firstName"), rs.getString("lastname"),
                        new Date(rs.getDate("birthDate").getTime()));
                //Actor actor = new Actor(rs.getLong("actor_id"), rs.getString("role"),rs.getLong("userId"));
                //actor.setUser(user);
                //account = new Account(rs.getLong("account_id"), rs.getString("username"), rs.getString("password"),
                    //    rs.getLong("actorId"));
                //account.setActor(actor);
                //if (account.getPassword().equals(password)){
                 //   found = true;
                //}
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return found ? account : null;
    }

    public void insertUser (String firstName, String lastName, Date birthdate){
        try {
            userInsertStatement.setString(1, firstName);
            userInsertStatement.setString(2,lastName);
            userInsertStatement.setDate(3,new java.sql.Date(birthdate.getTime()));
            userInsertStatement.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void printUser (Long id){

        User user = null;
        try {
            userSelectStatement.setLong(1, id);
            ResultSet rs = userSelectStatement.executeQuery();
            if (rs.next()){
                user = new User(rs.getLong("id"), rs.getString("firstName"), rs.getString("lastname"),
                        new Date(rs.getDate("birthDate").getTime()));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        System.out.println(user.getId() + " " + user.getFirstName() + " " + user.getLastName() + " " + user.getBirthDate());
    }

    private Long saveAccount(Account account) throws SQLException {
        Long id = null;
        accountInsertStatement.setString(1, account.getUsername());
        accountInsertStatement.setString(2, account.getPassword());
        accountInsertStatement.setLong(3, account.getActorId());
        accountInsertStatement.executeUpdate();
        ResultSet resultSet = accountInsertStatement.getGeneratedKeys();
        if (resultSet.next()) {
            id = resultSet.getLong(1);
        }
        return id;
    }

    private Long saveActor(Actor actor) throws SQLException {
        Long id = null;
        actorInsertStatement.setString(1,actor.getRole());
        actorInsertStatement.setLong(2, actor.getUserId());
        actorInsertStatement.executeUpdate();
        ResultSet resultSet = actorInsertStatement.getGeneratedKeys();
        if (resultSet.next()) {
            id = resultSet.getLong(1);
        }
        return id;
    }

    private Long saveUser(User user) throws SQLException {
        Long id = null;
        userInsertStatement.setString(1,user.getFirstName());
        userInsertStatement.setString(2, user.getLastName());
        userInsertStatement.setDate(3, new java.sql.Date(user.getBirthDate().getTime()));
        userInsertStatement.executeUpdate();
        ResultSet resultSet = userInsertStatement.getGeneratedKeys();
        if (resultSet.next()) {
            id = resultSet.getLong(1);
        }
        return id;
    }

    public void close(){
        try {
            //actorInsertStatement.close();
            //accountInsertStatement.close();
            userInsertStatement.close();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }
}
