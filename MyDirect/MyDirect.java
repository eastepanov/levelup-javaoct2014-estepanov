import java.io.*;
import java.util.Comparator;
import java.util.Set;
import java.util.Stack;
import java.util.TreeSet;

/**
 * Created by user on 18.11.2014.
 */
public class MyDirect implements Comparator<String> {

    private Set<String> fileSet = new TreeSet<>();
    private String mySrc;
    private String myDst;

    @Override
    public int compare(String s, String s2) {
        if(s.equals(s2) == true){
            return 1;
        }else
            return 0;
    }

    MyDirect(String src, String dst){
        this.mySrc = new String(src);
        this.myDst = new String(dst);
        File[] all = new File(mySrc).listFiles();
        viewMyDirection(new File(src));
        //viewDirection(new File(src));
    }

    private void viewDirection(File src){
        Stack<File> stackOnlyFiles = new Stack<File>();
        Stack<File> stackForLoop = new Stack<File>();

        stackForLoop.push(src);
        while(!stackForLoop.isEmpty()) {
            File child = stackForLoop.pop();
            if (child.isDirectory()) {
                for(File f : child.listFiles()) stackForLoop.push( f );
            } else if (child.isFile()) {
                fileSet.add(child.getName());
            }
        }
    }

    private void viewMyDirection(File src){
        //File[] all = (src).listFiles();
        if(src.isDirectory()){
            File[] all = (src).listFiles();
            for(int i = 0; i < all.length; ++i) {
                if (all[i].isDirectory()) {
                    viewDirection(all[i]);
                } else if (all[i].isFile()) {
                    fileSet.add(all[i].getName());
                }
            }
        }
    }

    public void print(){
        for(String s : fileSet){
            System.out.println(s);
        }
    }

   /* public void recordToDirection(){
        System.out.println("dsdsdsds");
        for(File f : fileSet){
            recordFiles(f,new File(clay(f.getName())));
        }
    }*/

    private String clay(String str){
        return new String(myDst + "\\" + str);
    }

    private void recordFiles(File src, File dst){
        System.out.println("225453356");
        try {
            InputStream in = new FileInputStream(src);
            OutputStream out = new FileOutputStream(dst);

            byte[] buffer = new byte[1024];
            int length;
            while((length = in.read(buffer)) > 0){
                out.write(buffer,0,length);
            }
            in.close();
            out.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }catch (IOException e){
            e.printStackTrace();
        }
    }


    public static void main(String[] args) {

        String source = new String("D:\\MyFirst");
        String destination =  new String("D:\\MyTempResMy");

        MyDirect myDirect = new MyDirect(source, destination);


        File fileSource = new File(source);
        File fileDst = new File(destination);

        if(!fileSource.exists()){
            System.out.println("Directory doesn`t exist!");
            System.exit(0);
        }else{
            if(!fileDst.exists())
                fileDst.mkdir();
        }

        myDirect.print();

        //myDirect.recordToDirection();
    }
}
