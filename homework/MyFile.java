package com.levelUp.homework;

import java.io.File;
import java.util.Comparator;

/**
 * Created by SMULL on 19.11.2014.
 */
public class MyFile implements Comparator<String>{

    static final String file = "FILE";
    private File nameFile;
    private String strSpace;

    public MyFile() {
    }

    public MyFile(File nameFile) {
        this.nameFile = nameFile;
    }


    @Override
    public int compare(String o1, String o2) {
        int k = o1.substring(o1.indexOf(" "))
                .compareTo(o2.substring(o2.indexOf(" ")));
        if(k == 0) {
            return o1.compareTo(o2);
        }
        else {
            return k;
        }
    }

    @Override
    public String toString() {
        if(MyFolder.myLength == nameFile.getName().length())
            return  nameFile.getName() + "\t" + file;
        else {
            strSpace = "";
            for (int i = 0; i < (MyFolder.myLength - nameFile.getName().length()); i++) {
                strSpace += " ";
            }
            return nameFile.getName() + strSpace + "\t" + file;
        }
    }
}
