package com.levelUp;

/**
 * Created by user on 25.11.2014.
 */

public class Part {

    private String idNumber;

    public String getIdNumber() {
        return idNumber;
    }

    public void setIdNumber(String idNumber) {
        this.idNumber = idNumber;
    }

    public Part() {
    }

    public Part(String idNumber) {
        this.idNumber = idNumber;
    }
}
